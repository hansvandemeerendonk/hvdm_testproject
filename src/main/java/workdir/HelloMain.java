package workdir;

/* This is a simple Java program.
FileName : "HelloWorld.java". */
public class HelloMain
{
    // Prints "Hello, World" to the terminal window.
    public static void main(String args[])
    {
        System.out.println("Hello, World");
        System.out.println("Hello from develop branch");
        System.out.println("Hello from BR2-branchFromDev");
        System.out.println("Hello from BR4-branchFromDevelop4");
        System.out.println("Hello from BR3-branchFromDevelop"); // manually in-between
        System.out.println("Hello from BR4-branchFromDevelop4 change2");
        System.out.println("Hello from BR4-branchFromDevelop4 change3");
        System.out.println("Hello from BR4-branchFromDevelop4 change4");
    }
}
